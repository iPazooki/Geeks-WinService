﻿using System;
using log4net;
using System.ServiceProcess;

namespace Geeks.WinService
{

    public partial class GeeksService : ServiceBase
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(GeeksService));
        public GeeksService()
        {
            InitializeComponent();
            CanHandlePowerEvent = true;
        }

        protected override void OnStart(string[] args)
        {
            _log.Info("Geeks windows service started.");
        }

        protected override void OnStop()
        {
            _log.Info("Geeks windows service stoped.");
        }

        protected override bool OnPowerEvent(PowerBroadcastStatus powerStatus)
        {
            if (powerStatus == PowerBroadcastStatus.Suspend)
            {
                try
                {
                    GeeksServices.SendGetRequest(_log);
                }
                catch (Exception e)
                {
                    _log.ErrorFormat("Error: {0}", e.Message);
                }
            }

            return base.OnPowerEvent(powerStatus);
        }
    }
}
