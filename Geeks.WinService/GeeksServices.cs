﻿using System;
using log4net;
using System.Text;
using System.Net;

namespace Geeks.WinService
{
    public class GeeksServices
    {
        public static void SendGetRequest(ILog log)
        {
            //init variables
            var machinNameAndUserName = $"{Environment.MachineName}|{Environment.UserName}";
            
            log.Info($"plain text: {machinNameAndUserName}");

            //encript variable
            var encryptVariable = Utility.Encrypt(machinNameAndUserName);
            
            log.Info($"encrypted text: {encryptVariable}");

            //decrypt variable
            var result = Utility.Decrypt(encryptVariable);
            
            log.Info($"decrypted text: {result}");

            //convert to base64string
            var stringBytes = Encoding.UTF8.GetBytes(encryptVariable);

            var base64String = Convert.ToBase64String(stringBytes);

            //Console.WriteLine($"base64: {base64String}");
            log.Info($"base64: {base64String}");

            //make a get request
            var url = $"https://dashboard.geeksltd.co.uk/@services/PcOff.ashx?token={base64String}";

            var webRequest = WebRequest.Create(url);

            //var proxy = new WebProxy("geeksProxy", 80) { BypassProxyOnLocal = true };

            //webRequest.Proxy = proxy;

            log.Info("Make Get request call");
            webRequest.GetResponse();

            //decode from base 64 and decrypt variable
            var base64ConvertedString = Convert.FromBase64String(base64String);

            var s = Encoding.UTF8.GetString(base64ConvertedString);

            var decrypt = Utility.Decrypt(s);
            
            log.Info($"decrept one from Base64: {decrypt}");
        }
    }
}