﻿using System.ServiceProcess;

namespace Geeks.WinService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new GeeksService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
